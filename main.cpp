#include <iostream>
#include <vector>
#include "functions.h"

/*
Copyright 2018 whitehousejanitor
https://gitlab.com/whitehousejanitor

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

int main()
{
    std::cout << "enter number, or any non-number to exit" << std::endl;
    int i = 0; // yeah whatever, it's bad practice, but i don't care
    std::vector<double> dataset(0); //note to self: you have to assign a size to vectors
    std::string a; // also bad practice
    double x; // yep
    while(true)
    {
        try
        {
            std::cin >> a;
            x = std::stoi(a);
            dataset.push_back(x);
            i++;
        }
        catch(std::exception)
        {
            break;
        }
    }
    std::vector<double> dataset2(0); //it works i guess
    for(int i2 = 0; i2 < i; i2++)
    {
        std::cout << dataset[i2] << ", ";
        dataset2.push_back(dataset[i2]);
    }
    std::cout << std::endl;
    std::vector<double> * datasetpointer = &dataset2;
    std::cout << "median: " << median(datasetpointer) << std::endl;
    std::cout << "mean: " << mean(datasetpointer) << std::endl;
    std::cout << "SD: " << standarddeviation(datasetpointer) << std::endl;
    return 0;
}
