#include "functions.h"
#include <cmath>
#include "math.h"
#include <vector>
#include <iostream>

/*
Copyright 2018 whitehousejanitor
https://gitlab.com/whitehousejanitor

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

double median(std::vector<double> * datasetpointer) // returns the middle value (or average of the two middle values)
{
    std::vector<double> dataset = *datasetpointer;
    if(floorf(dataset.size()/2.0)==dataset.size()/2.0)
    {
        return ((dataset[ceilf(dataset.size()/2.0)-1.0])*(dataset[ceilf(dataset.size()/2.0)]))/2.0;
    }
    else
    {
        return (dataset[ceilf(dataset.size()/2.0)-1]);
    }
    return -1;
}

double mean(std::vector<double> * datasetpointer) // returns the average
{
    std::vector<double> dataset = *datasetpointer;
    double sum = 0;
    for(int i = 0; i < dataset.size(); i++)
    {
        sum = sum + dataset[i]; //there's probably an efficient way to do this
    }
    return sum / dataset.size();
}

double mode(std::vector<double> * datasetpointer) // this is going to take a while
{
    std::vector<double> dataset = *datasetpointer; // TODO: fix this
    return 0;
}

double standarddeviation(std::vector<double> * datasetpointer)
{
    std::vector<double> dataset = *datasetpointer;
    std::vector<double> seconddataset(0);
    for(int i = 0; i < dataset.size(); i++)
    {
        seconddataset.push_back((dataset[i] - mean(&dataset)) * (dataset[i] - mean(&dataset))); // add the squared difference from mean
    }
    double sum;
    for(int i = 0; i <= seconddataset.size(); i++) // sum all of that
    {
        sum = sum + seconddataset[i]; //there's probably an efficient way to do this
    }
    return sqrt(sum / seconddataset.size());
}
